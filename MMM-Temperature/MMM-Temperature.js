Module.register('MMM-Temperature', {

        defaults: {
                prependString: 'Temperature: '
        },


        start: function() {
                this.temperature = 'fetching...';
                this.sendSocketNotification('CONNECT');
        },


        socketNotificationReceived: function(notification, temp) {
            if (notification === 'TEMPERATURE') {
                this.temperature = temp;
                this.updateDom();
            }
        },


        // Override dom generator.
        getDom: function() {
                var wrapper = document.createElement("div");
                wrapper.innerHTML = this.config.prependString + this.temperature;
                return wrapper;
        },

});