import RPi.GPIO as GPIO
import time
import requests

GPIO.setmode(GPIO.BCM)

ON_OFF = 0
URL_HDMI_ON = "http://localhost:8080/api/monitor/on"
URL_HDMI_OFF = "http://localhost:8080/api/monitor/off"

print "+-----------------------------------------------------------+"
print "|   Mesure de distance par le capteur ultrasonore HC-SR04   |"
print "+-----------------------------------------------------------+"

Trig = 23          # Entree Trig du HC-SR04 branchee au GPIO 23
Echo = 24         # Sortie Echo du HC-SR04 branchee au GPIO 24

GPIO.setup(Trig,GPIO.OUT)
GPIO.setup(Echo,GPIO.IN)

GPIO.output(Trig, False)

#repet = input("Entrez un nombre de repetitions de mesure : ")

#for x in range(repet):    # On prend la mesure "repet" fois
while True:

   time.sleep(1)       # On la prend toute les 1 seconde

   GPIO.output(Trig, True)
   time.sleep(0.00001)
   GPIO.output(Trig, False)

   while GPIO.input(Echo)==0:  ## Emission de l'ultrason
     debutImpulsion = time.time()

   while GPIO.input(Echo)==1:   ## Retour de l'Echo
     finImpulsion = time.time()

   distance = round((finImpulsion - debutImpulsion) * 340 * 100 / 2, 1)  ## Vitesse du son = 340 m/s

   if (distance <= 100):
     if (ON_OFF == 0):
       print "tu es trop proche"
       try:
       	    requests.get(url = URL_HDMI_ON)
            ON_OFF = 1
       except:
            print("Erreur d'appel a l api")
   else:
     if (ON_OFF == 1):
       print "tu es trop loin"
       try:
            requests.get(url = URL_HDMI_OFF)
            ON_OFF = 0
       except:
            print("Erreur d'appel a l api")

   print "La distance est de : ",distance," cm"

#GPIO.cleanup()
